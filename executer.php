<html>
<body>
<?php 
    /**
    * @author  ZXCoders
    * @since   29/11/2015
    * @license CC BY 4.0 https://creativecommons.org/licenses/by/4.0/
    */
    // Folder with python scripts to run
    $dir = '../../scriptPythonFolder/';

    // Execute selected script recived by POST
    if(isset($_POST['script'])) {
        try {
            system("python " .$dir. $_POST['script'], $res);
            echo "RUN: python " .$dir. $_POST['script'] . "<br>";
        } catch (Exception $e) {
            echo 'Exception: ',  $e->getMessage(), "<br>";
        }
    }

    $scripts = array();
    // Search for all pythons in folder
    if ($handle = opendir($dir)) {
        while (($file = readdir($handle)) !== false){
            if (!in_array($file, array('.', '..')) &&
                    !is_dir($dir.$file) &&
                    (new SplFileInfo($file))->getExtension() == 'py') {
                array_push($scripts,$file);
            }
        }
    }

    echo "<form action='executer.php' method='post'>";
    echo "<table>";
    foreach ($scripts as $script) {
        echo "<tr><td>";
        echo "<input type='radio' name='script' value='".$script."' checked> " . $script;
        echo "</td></tr>";
    }
    echo "<tr><td><input type='submit' value='Submit'></td></tr>";
    echo "</table>";
    echo "</form>";
?>
</body>
</html>
